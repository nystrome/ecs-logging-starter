package io.nystrome.logging.autoconfigure;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import co.elastic.logging.logback.EcsEncoder;
import co.elastic.logging.logback.EcsEncoder.Pair;
import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "logging.ecs")
public class SpringECSLoggingAutoConfiguration {

    private static final String ECS_APPENDER_NAME = "ECS_APPENDER";

    private boolean enabled = true;
    private boolean stackTraceAsArray = false;
    private boolean includeMarkers = false;
    private boolean includeOrigin = false;
    private List<Pair> additionalFields = new ArrayList<>();

    @Value("${spring.application.name:spring-boot-application}")
    private String serviceName;

    @Value("${LOG_FILE}")
    private String logFileName;

    @Value("${LOG_FILE_CLEAN_HISTORY_ON_START:false}")
    private boolean isCleanHistoryOnStart;

    @Value("${ROLLING_FILE_NAME_PATTERN:.%d{yyyy-MM-dd}.%i.gz}")
    private String rollingFileNamePattern;

    @Value("${LOG_FILE_MAX_SIZE:10MB}")
    private String logFileMaxSize;

    @Value("${LOG_FILE_MAX_HISTORY:7}")
    private int logFileMaxHistory;

    @Value("${LOG_FILE_TOTAL_SIZE_CAP:0}")
    private String logFileTotalSizeCap;

    @Bean
    @ConditionalOnProperty(value="logging.ecs.enabled", havingValue="true")
    public RollingFileAppender<ILoggingEvent> ecsAppender() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        EcsEncoder encoder = new EcsEncoder();
        encoder.setServiceName(serviceName);
        encoder.setStackTraceAsArray(stackTraceAsArray);
        encoder.setIncludeMarkers(includeMarkers);
        encoder.setIncludeOrigin(includeOrigin);
        additionalFields.forEach(pair -> encoder.addAdditionalField(pair));

        SizeAndTimeBasedRollingPolicy<ILoggingEvent> rollingPollicy = new SizeAndTimeBasedRollingPolicy<>();
        rollingPollicy.setCleanHistoryOnStart(isCleanHistoryOnStart);
        rollingPollicy.setFileNamePattern(logFileName + rollingFileNamePattern);
        rollingPollicy.setMaxFileSize(FileSize.valueOf(logFileMaxSize));
        rollingPollicy.setMaxHistory(logFileMaxHistory);
        rollingPollicy.setTotalSizeCap(FileSize.valueOf(logFileTotalSizeCap));

        RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<>();
        appender.setName(ECS_APPENDER_NAME);
        appender.setFile(logFileName);
        appender.setEncoder(encoder);
        appender.setRollingPolicy(rollingPollicy);

        encoder.start();
        appender.start();
        loggerContext.getLogger(Logger.ROOT_LOGGER_NAME).addAppender(appender);

        return appender;
    }

    
}
