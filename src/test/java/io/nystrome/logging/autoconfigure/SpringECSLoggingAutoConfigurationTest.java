package io.nystrome.logging.autoconfigure;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

import ch.qos.logback.core.rolling.RollingFileAppender;

public class SpringECSLoggingAutoConfigurationTest {

    private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(SpringECSLoggingAutoConfiguration.class));

    // @Test
    // @DisplayName("should not create a file appender and should fail (missing LOG_FILE property)")
    // public void contextFails(){
    //     this.contextRunner
    //         .withPropertyValues("logging.ecs.enabled=true")
    //         .run(context -> {
    //             // assertThat(context).hasFailed();
    //             // assertThat(context).getFailure().hasRootCauseInstanceOf("type");
    //             assertThat(context).doesNotHaveBean(RollingFileAppender.class);
    //         });
    // }
    
    @Test
    @DisplayName("should create a file appender with appropriate values")
    public void ecsFileAppenderLoadad(){
        this.contextRunner
            .withPropertyValues("logging.ecs.enabled=true")
            .withPropertyValues("LOG_FILE=my-file.json")
            .run(context -> {
                assertThat(context).hasNotFailed();
                assertThat(context).hasSingleBean(RollingFileAppender.class);
                assertThat(context.getBean(RollingFileAppender.class))
                    .hasFieldOrPropertyWithValue("name", "ECS_APPENDER")
                    .hasFieldOrPropertyWithValue("fileName", "my-file.json");
            });
    }

    @Test
    @DisplayName("should not create a file appender or fail")
    public void doesNotCreateEcsFileAppender(){
        this.contextRunner
            .run(context -> {
                assertThat(context).hasNotFailed();
                assertThat(context).doesNotHaveBean(RollingFileAppender.class);
            });
    }

    @Test
    @DisplayName("should not create a file appender or fail is explicitly set false")
    public void doesNotCreateEcsFileAppenderWhenFalse(){
        this.contextRunner
            .withPropertyValues("logging.ecs.enabled=false")
            .run(context -> {
                assertThat(context).hasNotFailed();
                assertThat(context).doesNotHaveBean(RollingFileAppender.class);
            });
    }
    
}
